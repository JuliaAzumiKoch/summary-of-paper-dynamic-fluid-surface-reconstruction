This repository contains: 
1. the paper Dynamic Fluid Surface Reconstruction Using Deep Neural Network by S. Thapa et al. presented at IEEE Conference on Computer Vision and Pattern Recognition (CVPR), pp. 21-30, 2020
2. a summary of this paper